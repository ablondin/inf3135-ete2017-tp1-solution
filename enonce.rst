Travail pratique 1
~~~~~~~~~~~~~~~~~~

Dans ce premier travail pratique, vous devrez concevoir un petit programme C
qui prend en entrée quatre arguments et qui dessine, en fonction de ces
arguments, un chemin dans un rectangle. Plus précisément, on s'attend à ce que
la commande

.. code:: bash

   ./tp1 <o> <v> <i> <c>

affiche sur ``stdout`` une représentation textuelle d'un chemin évoluant dans
un rectangle, où

- ``o`` est un caractère représentant une case occupée;
- ``v`` est un caractère représentant une case vide;
- ``i`` est un caractère représentant une intersection et
- ``c`` est une chaîne de caractère représentant une suite de déplacements
  selon les quatre points cardinaux (``E`` pour *est*, ``N`` pour *nord*, ``W``
  pour *ouest* et ``S`` pour *sud*).

Le travail doit être réalisé **seul**. Il doit être remis au plus tard le **4
juin 2017** à **23h59**. À partir de minuit, une pénalité de **2% par heure**
de retard sera appliquée.

Objectifs spécifiques
=====================

Les principaux objectifs visés sont les suivants :

- Vous initier au langage de **programmation C**;
- Vous initier au logiciel de contrôle de versions **Git**;
- Vous initier à la compilation d'un programme à l'aide d'un **Makefile**
  simple;
- Vous habituer à décomposer un programme en **petites fonctions**;
- Vous familiariser avec les fonctions dont les arguments sont des
  **pointeurs**.

Description du travail
======================

Votre programe devra prendre en entrée trois caractères (séparés par des
espaces) suivis d'une chaîne de caractère représentant un chemin utilisant les
quatre points cardinaux (``E``, ``N``, ``W`` et ``S``). Par exemple, si on
entre la commande

.. code:: bash

   ./tp1 X - + EEEENNWWSSSSS

on s'attend à ce que le chemin suivant soit affiché sur ``stdout`` (noter
que le point de départ se trouve à la 3e ligne, 1re colonne) :

.. code:: text

   --XXX
   --X-X
   XX+XX
   --X--
   --X--
   --X--

Ici, le caractère ``X`` représente une case occupée par le chemin, le caractère
``-`` représente une case vide et le caractère ``+`` représente un endroit où
le chemin se croise (c'est-à-dire qu'il y a une intersection). (On définit une
intersection comme une case occupée ayant 3 voisins ou plus qui sont aussi
occupés.)

Validation
==========

Lorsqu'une commande est lancée, il est possible que l'utilisateur ne respecte
pas certaines contraintes. Afin de vous assurer de la robustesse de votre
programme, vous devrez effectuer certaines vérifications pour qu'il n'ait pas
un comportement non souhaité :

1. Il doit toujours y avoir exactement 4 arguments passés en paramètres. Par
   exemple, la commande

   .. code:: bash

      ./tp1 X - + EENWWNEE a

   devrait afficher **tel quel** le message

   .. code:: bash

      Erreur: le nombre d'arguments est invalide

   sur ``stdout`` et la fonction ``main`` doit retourner le code d'erreur
   ``1``.
2. Les trois arguments représentant une case occupée, une case vide et une
   intersection doivent être des caractères uniques (bref, des chaînes de
   caractères de longueur 1). Par exemple, la commande

   .. code:: bash

      ./tp1 ab c d EENW

   devrait produire (toujours **tel quel**) le message

   .. code:: bash

      Erreur: les cases doivent etre identifiees par des caracteres

   et la fonction ``main`` doit retourner le code d'erreur ``2``.
3. Les trois caractères représentant une case occupée, une case vide et une
   intersection doivent être distincts deux à deux. Si on entre la commande

   .. code:: bash

      ./tp1 X X + EEN

   on s'attend à avoir le message

   .. code:: text

      Erreur: le caractere de case vide doit etre distinct

   et la fonction ``main`` doit retourner le code d'erreur ``3``.
4. Le chemin doit être une suite de caractères choisis parmi ``E``, ``N``,
   ``S`` et ``W``, en étant sensible à la casse (bref, la lettre minuscule
   ``e``, par exemple, n'est pas accepté). Ainsi, la commande

   .. code:: bash

      ./tp1 X - + EENeS

   entraîne le message

   .. code:: bash

      Erreur: les deplacements doivent etre E, N, S ou W

   et la fonction ``main`` doit retourner le code d'erreur ``4``.
5. La longueur de la chaîne représentant le chemin ne doit pas dépasser 40. En
   cas d'erreur, le message

   .. code:: bash

      Erreur: la longueur ne doit pas depasser 40

   doit être affiché et la fonction ``main`` doit retourner le code d'erreur
   ``5``.
6. La hauteur (le nombre de lignes) du chemin ne doit pas dépasser 10. En cas
   d'erreur, le message

   .. code:: bash

      Erreur: la hauteur ne doit pas depasser 10

   doit être affiché **tel quel** et la fonction ``main`` doit retourner le
   code d'erreur ``6``.
7. La largeur (le nombre de colonnes) du chemin ne doit pas dépasser 15. En cas
   d'erreur, le message

   .. code:: bash

      Erreur: la largeur ne doit pas depasser 15

   doit être affiché **tel quel** et la fonction ``main`` doit retourner le
   code d'erreur ``7``.
8. Le chemin doit toucher les quatre côtés du rectangle (il ne peut donc pas y
   avoir de colonnes ou de lignes vides.
9. L'affichage doit terminer par un retour à la ligne (caractère ``'\n'``).

Si toutes les contraintes sont vérifiées, alors le code ``0`` doit être
retourné pour indiquer que l'exécution est un succès.

Déclarations suggérées
======================

Dans ma solution, j'ai utilisé les structures de données et les prototypes
suivants :

.. code:: c

   // ---------- //
   // Constantes //
   // ---------- //
   
   #define MAX_LIGNES   10
   #define MAX_COLONNES 15
   #define MAX_CHEMIN   40
   #define EST          'E'
   #define NORD         'N'
   #define OUEST        'W'
   #define SUD          'S'

   // --------------------- //
   // Structures de données //
   // --------------------- //
   
   enum Code {                             // Code de validation des arguments
       CODE_OK                        = 0, // Tout est OK
       CODE_NOMBRE_ARGUMENTS_INVALIDE = 1, // Le nombre d'arguments est invalide
       CODE_CARACTERES_NON_UNIQUES    = 2, // Problème pour identifier les cases
       CODE_CARACTERE_VIDE_INVALIDE   = 3, // Le caractère vide est égal à un autre
       CODE_CHEMIN_INVALIDE           = 4, // Le chemin utilise des lettres invalides
       CODE_CHEMIN_TROP_LONG          = 5, // La longueur du chemin est trop grande
       CODE_HAUTEUR_INVALIDE          = 6, // La hauteur du chemin n'est pas valide
       CODE_LARGEUR_INVALIDE          = 7, // La largeur du chemin n'est pas valide
   };
   
   struct Rectangle { // Un rectangle à coordonnées entières
       int xmin;      // L'abscisse minimale
       int xmax;      // L'abscisse maximale
       int ymin;      // L'ordonnée minimale
       int ymax;      // L'ordonnée maximale
   };
   
   struct Dessin {                           // Un dessin
       unsigned int nbLignes;                // Le nombre de lignes
       unsigned int nbColonnes;              // Le nombre de colonnes
       char cases[MAX_LIGNES][MAX_COLONNES]; // Les cases du dessin
       char occupe;                          // Le caractère pour une case occupée
       char vide;                            // Le caractère pour une case vide
       char intersection;                    // Le caractère pour une intersection
   };
   
   struct Chemin {                        // Un chemin
       struct Rectangle rectangle;        // Le rectangle dans lequel s'inscrit le chemin
       char deplacements[MAX_CHEMIN + 1]; // Les déplacements
   };

   // ---------- //
   // Prototypes //
   // ---------- //
   
   // N'oubliez pas de documenter l'en-tête des prototypes!!!
   void afficherErreur(enum Code code);
   bool cheminEstValide(const char *chemin);
   void initialiserDessin(struct Dessin *dessin);
   void afficherDessin(const struct Dessin *dessin);
   void initialiserChemin(struct Chemin *chemin, const char *deplacements);
   void afficherChemin(const struct Chemin *chemin);
   void ajouterCheminDansDessin(struct Dessin *dessin, const struct Chemin *chemin);
   void identifierIntersections(struct Dessin *dessin);

Vous êtes évidemment libres d'utiliser d'autres constantes, d'autres structures
de données et d'autres prototypes si vous le souhaitez.

Contrôle de versions
====================

Votre projet devra être versionné à l'aide du logiciel Git. Vous serez en
particulier évalué sur votre façon de l'utiliser :

- Faites des *commits* de **petite taille**, réalisant une **tâche précise**.
  Ne faites surtout par l'erreur de faire un unique *commit* lors de la remise
  finale seulement.
- Assurez-vous d'écrire un message de *commit* **significatif**. Une première
  ligne dans laquelle vous décrivez de façon générale ce qui a été fait, puis
  les lignes suivantes donnant plus de détails si nécessaire.
- Il est obligatoire de **ne pas versionner avec Git** les fichiers inutiles ou
  trop volumineux, tels que :

  * les fichiers avec l'extension ``.o``;
  * le fichier exécutable ``tp1``;

- En revanche, vous devriez minimalement versionner le fichier ``README.md``,
  le ``Makefile`` ainsi que votre fichier ``tp1.c``.

Makefile
========

Il est obligatoire d'inclure un fichier ``Makefile`` dans votre projet pour en
faciliter la compilation. Celui-ci doit minimalement offrir les services
suivants :

- Lorsqu'on entre simplement ``make``, l'exécutable ``tp1`` doit être produit
  (ou mis à jour);
- Lorsqu'on entre ``make clean``, les fichiers ``.o`` et l'exécutable doivent
  être supprimés.
- Lorsqu'on entre ``make test``, une suite de tests est lancée pour valider
  votre programme.

Fichier README
==============

En plus du code source et du fichier ``Makefile``, votre projet doit contenir
un fichier nommé ``README.md`` qui en décrit le contenu et qui **respecte le
format Markdown**. Il doit minimalement contenir les informations ci-bas :

.. code:: md

   # Travail pratique 1

   ## Description

   Description du projet en quelques phrases
   Mentionner le contexte (cours, sigle, université, etc.)

   ## Auteur

   Prénom et nom (Code permanent)

   ## Fonctionnement

   Expliquez brièvement comment faire fonctionner votre projet, en inscrivant
   au moins deux exemples d'utilisation (commande lancée et résultat affiché).
   Assurez-vous que le code soit correctement formaté en suivant la syntaxe
   Markdown (les blocs de code suivent un format spécial).

   ## Contenu du projet

   Décrivez brièvement chacun des fichiers contenus dans le projet. Utilisez
   une liste à puce et décrivez-les de façon significative (une phrase par
   fichier)

   ## Références

   Citez vos sources ici, s'il y a lieu.

   ## Statut

   Indiquer si le projet est complété et sans bogue. Sinon, expliquez ce qui
   manque ou ce qui ne fonctionne pas.

Bien que la politique sur la qualité du français n'est pas systématiquement
appliquée dans vos cours, je l'appliquerai rigoureusement à ce fichier README.
Plus précisément, j'enlèverai **1%** par faute détectée, jusqu'à concurrence de
**10%**.

Style et documentation
======================

La qualité générale de votre code sera également évaluée. Assurez-vous de
respecter les contraintes de style habituelles : indentation, uniformité, noms
de variables et de fonctions significatifs. Évitez le mélange d'anglais et de
français, choisissez une des deux langues et tenez-vous-y. Évitez également les
lignes trop longues (une bonne pratique est de ne pas dépasser 80 caractères
par ligne, autant que possible).

Il faudra aussi documenter l'**en-tête** de votre fichier ``tp1.c`` ainsi que
chacun des **prototypes** (et non l'**implémentation**) de fonctions déclarés
en suivant le standard `Javadoc
<http://www.oracle.com/technetwork/articles/java/index-137868.html>`__.  Il est
aussi important de documenter les **constantes** et les **structures de
données** que vous définissez. Cependant, on ne devrait trouver **aucun
commentaire** dans le code et encore moins des **bouts de code** mis en
commentaires. Finalement, ne dupliquez pas les documentations d'en-tête de
fonctions : seul le **prototype** doit être documenté.

Contraintes additionnelles
==========================

Afin d'éviter des pénalités, il est important de respecter les contraintes
suivantes :

- Vous devez utiliser un **unique** fichier source nommé ``tp1.c``;
- Le nom de votre **exécutable** doit être ``tp1``;
- Les **prototypes** de toutes les fonctions doivent être déclarés vers le
  début du fichier avant d'être implémentés;
- **Aucune variable globale** (à l'exception des constantes);
- **Aucune bibliothèque (librairie)** autre que ``stdio``, ``string``,
  ``stdlib``, ``ctype`` et ``stdbool`` n'est permise;
- Il est interdit de **copier intégralement** des bouts de code provenant
  d'autres sources, même en mentionnant leur source (en revanche, vous pouvez
  vous **inspirer** d'autre code en citant sa provenance).
- **Aucune allocation dynamique** n'est permise (fonctions ``malloc``,
  ``calloc`` et ``free``);
- Votre programme doit **compiler** sans **erreur** et sans **avertissement**
  avec l'option ``-W -Wall``.

Advenant le non-respect d'une ou plusieurs de ces contraintes, des pénalités
**pouvant aller jusqu'à l'attribution de la note 0** pourraient être
appliquées.

Correction automatique
======================

Il sera possible en tout temps de vérifier si votre programme passe différents
tests de validation, de façon automatique. Pour cela, il vous suffit d'ajouter
les deux fichiers suivants à votre projet **sans les modifier** :

- `test.py <../files/inf3135/ete2017/tp1/test.py>`__, qui contient une
  suite de tests écrite en Python (vous n'avez pas besoin de connaître ce
  langage pour la faire fonctionner);
- `gitlab-ci.yml <../files/inf3135/ete2017/tp1/gitlab-ci.yml>`__, qui indique à
  GitLab comment lancer de façon automatique la suite de tests (assurez-vous de
  renommer le fichier ``.gitlab-ci.yml`` avec un point au début sinon cela ne
  fonctionnera pas).

Notez que ces deux fichiers testeront votre projet chaque fois que vous
effectuerez un commit et que vous le pousserez sur votre dépôt GitLab. Si vous
souhaitez effectuer des tests localement, il vous faudra vous assurer que
Python 2 (pas Python 3) soit installé sur votre machine.

Remise
======

Votre travail doit être remis au plus tard le **4 juin 2017** à **23h59**.  À
partir de minuit, une pénalité de **2% par heure** de retard sera appliquée.

La remise se fait **obligatoirement** par l'intermédiaire de la plateforme
`GitLab <https://about.gitlab.com/>`__.  **Aucune remise par courriel ne sera
acceptée** (le travail sera considéré comme non remis).

Le nom de votre dépôt doit être (en minuscules) ``inf3135-ete2017-tp1``. Vous
devez donner un accès en mode **Developer** (pas **Master**) à l'utilisateur
``ablondin`` (moi-même). Ceci me permettra de déposer directement dans vos
projets votre note pour le travail ainsi que mes commentaires.

Votre projet devrait minimalement contenir les fichiers suivants :

- Un fichier ``tp1.c`` contenant le code source de votre projet, ainsi que
  votre fonction ``main``.
- Un fichier ``README.md``;
- Un fichier ``Makefile`` supportant les appels ``make``, ``make clean`` et
  ``make test``;
- Un fichier ``.gitignore``;
- Les fichiers `test.py <../files/inf3135/ete2017/tp1/test.py>`__ et
  `gitlab-ci.yml <../files/inf3135/ete2017/tp1/gitlab-ci.yml>`__ (**ne les
  modifiez pas**) que je vous fournis.

Les travaux seront corrigés sur le serveur Malt. Vous devez donc vous assurer
que votre programme fonctionne **sans modification** sur celui-ci.

Barème
======

+--------------------+---------------------------------+-----------+
| Critère            | Description                     | Points    |
+====================+=================================+===========+
|                    | Affichage du dessin,            |           |
| Fonctionnabilité   | affichage du chemin,            | 40        |
|                    | intersections, validation       |           |
+--------------------+---------------------------------+-----------+
|                    | Décompositions en fonctions     |           |
| Qualité du code    |                                 | 30        |
|                    | Style et documentation          |           |
+--------------------+---------------------------------+-----------+
|                    | Respect du format Markdown,     |           |
| Fichier README     | qualité du contenu              | 10        |
|                    |                                 |           |
+--------------------+---------------------------------+-----------+
|                    | Compilation, nettoyage et       |           |
| Makefile           | tests (``make``, ``make clean`` | 10        |
|                    | et ``make test``)               |           |
+--------------------+---------------------------------+-----------+
|                    | Plusieurs *commits* liés à une  |           |
| Utilisation de Git | tâche spécifique, message clair | 10        |
|                    | et pertinent                    |           |
+--------------------+---------------------------------+-----------+
| **Total**                                            | **100**   |
+--------------------+---------------------------------+-----------+

