/**
 * Solution du travail pratique 1 donné dans le cadre du cours INF3135
 * Construction et maintenance de logiciels, que j'enseigne à l'été 2017.
 *
 * Le travail ne contient qu'un seul module nommé `tp1`. Il s'agit d'un petit
 * programme qui permet d'afficher un chemin évoluant dans un rectangle.
 *
 * Voir le fichier `README.md` pour plus d'informations. Un fichier `Makefile`
 * est également inclus pour simplifier la compilation, le nettoyage et le
 * lancement de tests automatiques.
 *
 * @author  Alexandre Blondin Massé
 * @date    9 juin 2017
 */
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

// ---------- //
// Constantes //
// ---------- //

#define MAX_LIGNES   10
#define MAX_COLONNES 15
#define MAX_CHEMIN   40
#define EST          'E'
#define NORD         'N'
#define OUEST        'W'
#define SUD          'S'

// --------------------- //
// Structures de données //
// --------------------- //

enum Code {                             // Code de validation des arguments
    CODE_OK                        = 0, // Tout est OK
    CODE_NOMBRE_ARGUMENTS_INVALIDE = 1, // Le nombre d'arguments est invalide
    CODE_CARACTERES_NON_UNIQUES    = 2, // Problème pour identifier les cases
    CODE_CARACTERE_VIDE_INVALIDE   = 3, // Le caractère vide est égal à un autre
    CODE_CHEMIN_INVALIDE           = 4, // Le chemin utilise des lettres invalides
    CODE_CHEMIN_TROP_LONG          = 5, // Le chemin utilise des lettres invalides
    CODE_HAUTEUR_INVALIDE          = 6, // La hauteur du chemin n'est pas valide
    CODE_LARGEUR_INVALIDE          = 7, // La largeur du chemin n'est pas valide
};

struct Rectangle { // Un rectangle à coordonnées entières
    int xmin;      // L'abscisse minimale
    int xmax;      // L'abscisse maximale
    int ymin;      // L'ordonnée minimale
    int ymax;      // L'ordonnée maximale
};

struct Dessin {                           // Un dessin
    unsigned int nbLignes;                // Le nombre de lignes
    unsigned int nbColonnes;              // Le nombre de colonnes
    char cases[MAX_LIGNES][MAX_COLONNES]; // Les cases du dessin
    char occupe;                          // Le caractere pour une case occupee
    char vide;                            // Le caractere pour une case vide
    char intersection;                    // Le caractere pour une intersection
};

struct Chemin {                        // Un chemin
    struct Rectangle rectangle;        // Le rectangle dans lequel s'inscrit le chemin
    char deplacements[MAX_CHEMIN + 1]; // Les déplacements
};

// ---------- //
// Prototypes //
// ---------- //

/**
 * Affiche un message d'erreur en fonction du code d'erreur.
 *
 * @param code  Le code d'erreur
 */
void afficherErreur(enum Code code);

/**
 * Retourne vrai si le chemin donné est valide.
 *
 * Un chemin est valide s'il n'est composé que des lettres E, N, W et S.
 *
 * @param chemin  Le chemin à valider
 * @return        Vrai si le chemin est valide
 */
bool cheminEstValide(const char *chemin);

/**
 * Initialise un dessin avec des cases vides.
 *
 * @param dessin  Le dessin à initialiser
 */
void initialiserDessin(struct Dessin *dessin);

/**
 * Affiche un dessin sur stdout.
 *
 * @param dessin  Le dessin à afficher
 */
void afficherDessin(const struct Dessin *dessin);

/**
 * Initialise un chemin à partir de déplacements.
 *
 * @param chemin        Le chemin à initialiser
 * @param deplacements  Une suite de déplacements sur
 *                      l'alphabet {d, h, g, b}
 */
void initialiserChemin(struct Chemin *chemin,
                       const char *deplacements);

/**
 * Affiche un chemin sur stdout.
 *
 * @param chemin  Le chemin à afficher
 */
void afficherChemin(const struct Chemin *chemin);

/**
 * Ajoute le chemin donné dans un dessin
 *
 * @param dessin  Le dessin auquel on ajoute un chemin
 * @param chemin  Le chemin à ajouter
 */
void ajouterCheminDansDessin(struct Dessin *dessin,
                             const struct Chemin *chemin);

/**
 * Identifie les points d'intersection.
 *
 * @param dessin  Le dessin dans lequel on fait l'identification
 */
void identifierIntersections(struct Dessin *dessin);

// ---- //
// Main //
// ---- //

int main(int argc, char *argv[]) {
    struct Dessin dessin;
    struct Chemin chemin;
    enum Code code;

    if (argc != 5) {
        code = CODE_NOMBRE_ARGUMENTS_INVALIDE;
    } else if (strlen(argv[1]) != 1 || strlen(argv[2]) != 1 || strlen(argv[3]) != 1) {
        code = CODE_CARACTERES_NON_UNIQUES;
    } else if (argv[1][0] == argv[2][0] || argv[3][0] == argv[2][0]) {
        code = CODE_CARACTERE_VIDE_INVALIDE;
    } else if (strlen(argv[4]) > MAX_CHEMIN) {
        code = CODE_CHEMIN_TROP_LONG;
    } else if (!cheminEstValide(argv[4])) {
        code = CODE_CHEMIN_INVALIDE;
    } else {
        initialiserChemin(&chemin, argv[4]);
        dessin.nbLignes = chemin.rectangle.ymax - chemin.rectangle.ymin + 1;
        dessin.nbColonnes = chemin.rectangle.xmax - chemin.rectangle.xmin + 1;
        if (dessin.nbLignes > MAX_LIGNES) {
            code = CODE_HAUTEUR_INVALIDE;
        } else if (dessin.nbColonnes > MAX_COLONNES) {
            code = CODE_LARGEUR_INVALIDE;
        } else {
            dessin.occupe = argv[1][0];
            dessin.vide = argv[2][0];
            dessin.intersection = argv[3][0];
            initialiserDessin(&dessin);
            ajouterCheminDansDessin(&dessin, &chemin);
            identifierIntersections(&dessin);
            afficherDessin(&dessin);
            code = CODE_OK;
        }
    }
    afficherErreur(code);
    return code;
}

// -------------- //
// Implémentation //
// -------------- //

void afficherErreur(enum Code code) {
    switch (code) {
        case CODE_NOMBRE_ARGUMENTS_INVALIDE:
            printf("Erreur: le nombre d'arguments est invalide\n");
            break;
        case CODE_CARACTERES_NON_UNIQUES:
            printf("Erreur: les cases doivent etre identifiees par des caracteres\n");
            break;
        case CODE_CARACTERE_VIDE_INVALIDE:
            printf("Erreur: le caractere de case vide doit etre distinct\n");
            break;
        case CODE_CHEMIN_INVALIDE:
            printf("Erreur: les deplacements doivent etre E, N, S ou W\n");
            break;
        case CODE_CHEMIN_TROP_LONG:
            printf("Erreur: la longueur ne doit pas depasser %d\n", MAX_CHEMIN);
            break;
        case CODE_HAUTEUR_INVALIDE:
            printf("Erreur: la hauteur ne doit pas depasser %d\n", MAX_LIGNES);
            break;
        case CODE_LARGEUR_INVALIDE:
            printf("Erreur: la largeur ne doit pas depasser %d\n", MAX_COLONNES);
            break;
        case CODE_OK:
            break;
    }
}

bool cheminEstValide(const char *chemin) {
    unsigned int i = 0;
    while (chemin[i] != '\0') {
        if (chemin[i] != EST   && chemin[i] != NORD &&
            chemin[i] != OUEST && chemin[i] != SUD) {
            return false;
        }
        ++i;
    }
    return true;
}

void initialiserDessin(struct Dessin *dessin) {
    for (unsigned int i = 0; i < dessin->nbLignes; ++i) {
        for (unsigned int j = 0; j < dessin->nbColonnes; ++j) {
            dessin->cases[i][j] = dessin->vide;
        }
    }
}

void afficherDessin(const struct Dessin *dessin) {
    for (unsigned int i = 0; i < dessin->nbLignes; ++i) {
        for (unsigned int j = 0; j < dessin->nbColonnes; ++j) {
            printf("%c", dessin->cases[i][j]);
        }
        printf("\n");
    }
}

void initialiserChemin(struct Chemin *chemin,
                       const char *deplacements) {
    strncpy(chemin->deplacements, deplacements, MAX_CHEMIN);
    int xmin = 0, xmax = 0, ymin = 0, ymax = 0;
    int x = 0, y = 0;
    unsigned int i = 0;
    while (deplacements[i] != '\0') {
        char c = deplacements[i];
        switch (c) {
            case EST:   ++x; break;
            case NORD:  --y; break;
            case OUEST: --x; break;
            case SUD:   ++y; break;
        }
        xmin = x < xmin ?  x : xmin;
        xmax = x > xmax ?  x : xmax;
        ymin = y < ymin ?  y : ymin;
        ymax = y > ymax ?  y : ymax;
        ++i;
    }
    chemin->rectangle.xmin = xmin;
    chemin->rectangle.xmax = xmax;
    chemin->rectangle.ymin = ymin;
    chemin->rectangle.ymax = ymax;
}

void afficherChemin(const struct Chemin *chemin) {
    printf("Chemin: %s\n  Rectangle: (%d, %d, %d, %d)\n",
           chemin->deplacements,
           chemin->rectangle.xmin,
           chemin->rectangle.ymin,
           chemin->rectangle.xmax,
           chemin->rectangle.ymax);
}

void ajouterCheminDansDessin(struct Dessin *dessin,
                             const struct Chemin *chemin) {
    unsigned int i = 0;
    int x = -chemin->rectangle.xmin;
    int y = -chemin->rectangle.ymin;
    while (chemin->deplacements[i] != '\0') {
        dessin->cases[y][x] = dessin->occupe;
        switch (chemin->deplacements[i]) {
            case EST:   ++x; break;
            case NORD:  --y; break;
            case OUEST: --x; break;
            case SUD:   ++y; break;
        }
        ++i;
    }
    dessin->cases[y][x] = dessin->occupe;
}

void identifierIntersections(struct Dessin *dessin) {
    for (unsigned int i = 0; i < dessin->nbLignes; ++i) {
        for (unsigned int j = 0; j < dessin->nbColonnes; ++j) {
            unsigned int nbVoisins = 0;
            if (i > 0 && dessin->cases[i - 1][j] != dessin->vide) {
                ++nbVoisins;
            }
            if (i < dessin->nbLignes - 1 && dessin->cases[i + 1][j] != dessin->vide) {
                ++nbVoisins;
            }
            if (j > 0 && dessin->cases[i][j - 1] != dessin->vide) {
                ++nbVoisins;
            }
            if (j < dessin->nbColonnes - 1 && dessin->cases[i][j + 1] != dessin->vide) {
                ++nbVoisins;
            }
            if (dessin->cases[i][j] == dessin->occupe && nbVoisins >= 3) {
                dessin->cases[i][j] = dessin->intersection;
            }
        }
    }
}
