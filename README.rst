Solution du travail pratique 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ce dépôt contient la solution au travail pratique 1 du cours INF3135
Construction et maintenance de logiciel que j'enseigne à l'été 2017.

Objectif
========

L'objectif de ce travail est de produire un petit programme C nommé ``tp1`` qui
permet de dessiner un chemin dans un rectangle. Plus précisément, on s'attend à
ce que la commande

.. code:: text

   ./tp1 <o> <v> <i> <c>

affiche sur ``stdout`` une représentation textuelle d'un chemin évoluant dans
un rectangle, où

- ``o`` est un caractère (type ``char``) représentant une case occupée;
- ``v`` est un caractère (type ``char``) représentant une case vide;
- ``i`` est un caractère (type ``char``) représentant une intersection et
- ``c`` est une chaîne de caractère (type ``char *``) représentant une suite de
  déplacements selon les quatre points cardinaux (``E`` pour *est*, ``N`` pour
  *nord*, ``W`` pour *ouest* et ``S`` pour *sud*).

Auteur
======

Alexandre Blondin Massé

Exemple
=======

Si on entre la commande

.. code:: text

   ./tp1 X - + EEEENNWWSSSSS

le chemin suivant soit affiché sur ``stdout`` (noter que le point de départ se
trouve à la 3e ligne, 1re colonne) :

.. code:: text

   --XXX
   --X-X
   XX+XX
   --X--
   --X--
   --X--

Ici, le caractère ``X`` représente une case occupée par le chemin, le caractère
``-`` représente une case vide et le caractère ``+`` représente un endroit où
le chemin se croise (c'est-à-dire qu'il y a une intersection). (On définit une
intersection comme une case occupée ayant 3 voisins ou plus qui sont aussi
occupés.)

Énoncé
======

L'énoncé du travail est décrit plus en détails dans le fichier `enonce.rst` du
même répertoire.
