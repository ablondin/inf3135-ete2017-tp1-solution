EXEC = tp1

$(EXEC): tp1.o
	gcc -o $(EXEC) tp1.o

tp1.o: tp1.c
	gcc -o tp1.o -std=c99 -c tp1.c

.PHONY: clean html test

clean:
	rm -f *.o $(EXEC)
	rm -f *.html

html:
	pandoc -s README.rst -o README.html
	pandoc -s enonce.rst -o enonce.html

test: $(EXEC)
	python test.py
